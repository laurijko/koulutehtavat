/**
 * Created by late on 7.5.2017.
 */
public class Client {
    /**
     * to get raw materials
     */
    public static void main(String[] args) {
        StoreKeeper keeper = new StoreKeeper();
        RawMaterialGoods rawMaterialGoods = keeper.getRawMaterialGoods();

        //Or

        PackingMaterialGoods packingMaterialGoods = (PackingMaterialGoods) keeper.getGoods("Packaging");
        FinishedGoods finishedGoods = (FinishedGoods) keeper.getGoods("Finished");

    }
}
