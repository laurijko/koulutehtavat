package kello;

/**
 * Created by late on 1.5.2017.
 */
public class Kello implements Cloneable {
    private Viisari tuntiViisari, minuuttiViisari, sekuntiViisari;

    public Kello(){
        tuntiViisari = new Viisari(23);
        minuuttiViisari = new Viisari(59);
        sekuntiViisari = new Viisari(59);
        sekuntiViisari.addNotifioitava(minuuttiViisari);
        minuuttiViisari.addNotifioitava(tuntiViisari);
    }

    public void printAika(){
        System.out.println(tuntiViisari.getAika()+":"+minuuttiViisari.getAika()+
                ":"+sekuntiViisari.getAika());
    }

    public void setTunti(int aika){
        tuntiViisari.setAika(aika);
    }

    public void setMinuutti(int aika){
        minuuttiViisari.setAika(aika);
    }

    public void setSekunti(int aika){
        sekuntiViisari.setAika(aika);
    }

    public void siirräTunti(int aika){
        tuntiViisari.siirrä(aika);
    }

    public void siirräMinuutti(int aika){
        minuuttiViisari.siirrä(aika);
    }

    public void siirräSekunti(int aika){
        sekuntiViisari.siirrä(aika);
    }

    public Object matalaKopio(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public Object syväKopio(){
        Kello k = null;
        try {
            k = (Kello)super.clone();
            k.minuuttiViisari = (Viisari)minuuttiViisari.clone();
            k.tuntiViisari = (Viisari)tuntiViisari.clone();
            k.sekuntiViisari = (Viisari)sekuntiViisari.clone();
            k.sekuntiViisari.addNotifioitava(k.minuuttiViisari);
            k.minuuttiViisari.addNotifioitava(k.tuntiViisari);
        } catch (CloneNotSupportedException e) {}
        return k;
    }
}
