package kello;

import java.util.Observable;

/**
 * Created by late on 1.5.2017.
 */
public class Viisari implements Cloneable {
    private int aika = 0;
    private int max;
    private Viisari notifioitava;

    public Viisari(int max){
        this.max = max;
    }

    protected void tick() {
        if(aika<max) {
            aika++;
        } else {
            aika=0;
            if(notifioitava!=null){
                notifioitava.tick();
            }
        }
    }

    protected void setAika(int aika){
        this.aika = aika;
    }

    protected void siirrä(int aika){
        for(int i=0; i < aika; i++){
            tick();
        }
    }

    protected int getAika(){
        return aika;
    }

    protected void addNotifioitava(Viisari viisari){
        notifioitava = viisari;
    }

    protected Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
