/**
 * Created by late on 1.5.2017.
 */
import kello.Kello;

public class main {
    public static void main(String[] args) {
        Kello kello = new Kello();
        System.out.println("kellon siirto ja matala kopio");
        kello.printAika();
        kello.siirräSekunti(124523);
        kello.printAika();
        Kello matalaKlooni = (Kello)kello.matalaKopio();
        matalaKlooni.printAika();

        System.out.println("");
        System.out.println("kloonin kellon siirto");
        matalaKlooni.siirräSekunti(245232);
        kello.printAika();
        matalaKlooni.printAika();

        System.out.println("");
        System.out.println("Syväkopiointi");
        Kello syväKlooni = (Kello)kello.syväKopio();
        syväKlooni.printAika();

        System.out.println("");
        System.out.println("Syväkopion kellon siirto");
        syväKlooni.siirräSekunti(12342);
        kello.printAika();
        syväKlooni.printAika();
    }
}
