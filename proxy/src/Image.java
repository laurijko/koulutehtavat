/**
 * Created by lauri on 18-Apr-17.
 */
public interface Image {
    public void displayImage();
    public void showData();
}
