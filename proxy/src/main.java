import java.util.ArrayList;

/**
 * Created by lauri on 18-Apr-17.
 */
public class main {
    /**
     * Test method
     */
    public static void main(final String[] arguments) {
        ArrayList<Image> valokuvakansio = new ArrayList();
        valokuvakansio.add(new ProxyImage("HiRes_10MB_Photo1"));
        valokuvakansio.add(new ProxyImage("HiRes_10MB_Photo2"));

        System.out.println("Kuvien nimet:");
        for(Image kuva : valokuvakansio){ kuva.showData();}
        System.out.println("\nNäytetään ja ladataan kuvat:");
        for(Image kuva : valokuvakansio){ kuva.displayImage();}
        System.out.println("\nNäytetään kuvat");
        for(Image kuva : valokuvakansio){ kuva.displayImage();}
    }
}
