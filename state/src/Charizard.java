public class Charizard extends Pokemon_State {

    public Charizard() {
        name = "CHARIZARD";
        attack = "FLAMETHROWER";
        type = "[FIRE] [FLYING]";
        hp = 78;
        att = 84;
        def = 78;
        spec = 85;
        spd = 100;
        lvl = 36;
    }


    @Override
    void levelup(Pokemon p) {
        lvl++;
        hp += 3;
        att += 3;
        def += 3;
        spec += 3;
        spd += 3;

        System.out.println(name + " grew to level " + lvl + "!");
        stats();
    }
}
