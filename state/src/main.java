import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Pokemon pokemon = new Pokemon();

        boolean exit = false;
        Scanner s = new Scanner(System.in);

        while(!exit){
            System.out.println("");
            System.out.println("\t\t\t\t\t1. FIGHT");
            System.out.println("\t\t\t\t\t2. RUN");
            System.out.println("\t\t\t\t\t3. STATS");
            System.out.println("\t\t\t\t\t4. LVLUP");
            System.out.println("\t\t\t\t\t5. EXIT");
            System.out.print("\t\t\t\t\tEnter a number: ");
            String input = s.nextLine();
            System.out.println("");
            switch (input){
                case "1":
                    pokemon.fight();
                    break;
                case "2":
                    pokemon.run();
                    break;
                case "3":
                    pokemon.stats();
                    break;
                case "4":
                    pokemon.levelup();
                    break;
                case "5":
                    exit = true;
                    break;
            }
        }
    }
}
