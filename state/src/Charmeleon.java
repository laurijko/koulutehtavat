public class Charmeleon extends Pokemon_State {

    public Charmeleon() {
        name = "CHARMELEON";
        attack = "EMBER";
        type = "[FIRE]";
        hp = 58;
        att = 64;
        def = 58;
        spec = 65;
        spd = 80;
        lvl = 16;
    }

    @Override
    void levelup(Pokemon p) {
        lvl++;
        hp += 1;
        att += 1;
        def += 1;
        spec += 1;
        spd += 1;
        System.out.println(name + " grew to level " + lvl + "!");
        if(lvl == 36){
            System.out.println("What? " + name + " is evolving!");
            System.out.println(name + " evolved into CHARIZARD!");
            p.state = new Charizard();
        } else {
            stats();
        }
    }
}
