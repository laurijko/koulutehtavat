import java.util.Random;

public abstract class Pokemon_State {
    Random random = new Random();
    String name;
    String attack;
    String type;
    int lvl;
    int hp;
    int att;
    int def;
    int spec;
    int spd;
    private int runTries = 0;

    public void fight(){
        System.out.println(name + " used " + attack + "!");
    }

    public void run() {
        if(random.nextInt(255) < ((spd*32)/50)+30*runTries) {
            System.out.println("Got away safely!");
            runTries = 0;
        } else {
            System.out.println("Can't Escape.");
            runTries++;
        }
    }

    public void stats() {
        System.out.println(name + ":");
        System.out.println("TYPE: "+type);
        System.out.println("LVL: "+lvl);
        System.out.println("HP: "+hp);
        System.out.println("ATT: "+att);
        System.out.println("DEF: "+def);
        System.out.println("SPEC: "+spec);
        System.out.println("SPD: "+spd);
    }

    abstract void levelup(Pokemon p);

}
