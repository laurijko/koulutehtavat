public class Charmander extends Pokemon_State {

    public Charmander() {
        name = "CHARMANDER";
        attack = "SCRATCH";
        type = "[FIRE]";
        hp = 39;
        att = 52;
        def = 43;
        spec = 50;
        spd = 65;
        lvl = 1;
    }

    @Override
    void levelup(Pokemon p) {
        lvl++;
        hp += 1;
        att += 1;
        def += 1;
        spec += 1;
        spd += 1;
        System.out.println(name + " grew to level " + lvl + "!");
        if(lvl==16){
            System.out.println("What? " + name + " is evolving!");
            System.out.println(name + " evolved into CHARMELEON!");
            p.state = new Charmeleon();
        } else {
            stats();
        }

    }
}
