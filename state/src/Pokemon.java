/**
 * Created by late on 10.4.2017.
 */
public class Pokemon {
    Pokemon_State state;

    public Pokemon(){
        state = new Charmander();
    }

    public void fight(){
        state.fight();
    }

    public void run(){
        state.run();
    }

    public void stats(){
        state.stats();
    }

    public void levelup(){
        state.levelup(this);
    }

}
