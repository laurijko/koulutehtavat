/**
 * Created by late on 22.4.2017.
 */
public class main {
    public static void main(String[] args) {
        Pelihahmo[] pelaajat = {new AkuAnkka(), new HannuHanhi()};

        System.out.println("\nRound 1");
        for(Pelihahmo p : pelaajat){
            p.accept(PisteytysVisitor.getINSTANCE());
        }

        System.out.println("\nRound 2");
        pelaajat[0].setTila(Parempi.getInstance());
        pelaajat[1].setTila(Paras.getInstance());
        for(Pelihahmo p : pelaajat){
            p.accept(PisteytysVisitor.getINSTANCE());
        }

        System.out.println("\nRound 3");
        pelaajat[0].setTila(Paras.getInstance());
        pelaajat[1].setTila(Hyvä.getInstance());
        for(Pelihahmo p : pelaajat){
            p.accept(PisteytysVisitor.getINSTANCE());
        }

    }
}
