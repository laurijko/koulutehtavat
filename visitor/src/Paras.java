/**
 * Created by late on 22.4.2017.
 */
public class Paras extends Tila {
    private static Paras INSTANCE = null;

    private Paras(){
    }

    public static Paras getInstance(){
        if(INSTANCE == null)
            INSTANCE = new Paras();
        return INSTANCE;
    }
    @Override
    int getBonusPoints() {
        return 3;
    }

    @Override
    public String toString() {
        return "Paras";
    }
}
