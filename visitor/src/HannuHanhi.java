/**
 * Created by late on 22.4.2017.
 */
public class HannuHanhi extends Pelihahmo {
    @Override
    int getBonusPoints() {
        return 1;
    }

    public HannuHanhi() {
        super("Hannu Hanhi");
    }
}
