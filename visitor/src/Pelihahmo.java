/**
 * Created by late on 22.4.2017.
 */
public abstract class Pelihahmo {
    private String nimi;
    private Tila tila;

    abstract int getBonusPoints();

    void accept (PisteytysVisitor visitor) {
        visitor.visit(this);
    }

    public Pelihahmo(){};

    public Pelihahmo(String nimi) {
        this.nimi = nimi;
    }

    public Tila getTila() {
        return tila;
    }

    public void setTila(Tila tila) {
        this.tila = tila;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
}
