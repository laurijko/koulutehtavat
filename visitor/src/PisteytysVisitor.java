/**
 * Created by late on 22.4.2017.
 */
public class PisteytysVisitor {
    private static PisteytysVisitor INSTANCE;
    private PisteytysVisitor(){};

    public static PisteytysVisitor getINSTANCE(){
        if(INSTANCE == null)
            INSTANCE = new PisteytysVisitor();
        return INSTANCE;
    }

    void visit(Pelihahmo hahmo){
        String nimi = hahmo.getNimi();
        Tila tila = hahmo.getTila();
        String tuloste = "";
        int pisteet = 0;

        pisteet += hahmo.getBonusPoints();
        if(tila!=null) {
            pisteet += tila.getBonusPoints();
            tuloste += tila + " ";
        }

        System.out.println(tuloste + nimi+" sai "+pisteet+" bonus pistettä");

    }
}
