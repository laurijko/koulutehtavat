/**
 * Created by late on 22.4.2017.
 */
public class Parempi extends Tila {

    private static Parempi INSTANCE = null;

    private Parempi(){
    }

    public static Parempi getInstance(){
        if(INSTANCE == null)
            INSTANCE = new Parempi();
        return INSTANCE;
    }

    @Override
    int getBonusPoints() {
        return 2;
    }

    @Override
    public String toString() {
        return "Parempi";
    }
}
