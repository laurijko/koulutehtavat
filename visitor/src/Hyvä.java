/**
 * Created by late on 22.4.2017.
 */
public class Hyvä extends Tila {
    private static Hyvä INSTANCE = null;

    private Hyvä(){
    }

    public static Hyvä getInstance(){
        if(INSTANCE == null)
            INSTANCE = new Hyvä();
        return INSTANCE;
    }

    @Override
    int getBonusPoints() {
        return 1;
    }

    @Override
    public String toString() {
        return "Hyvä";
    }
}
