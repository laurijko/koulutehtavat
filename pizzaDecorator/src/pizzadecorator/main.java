/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzadecorator;

/**
 *
 * @author lauri
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pizza salamipizza = new Juusto(new Tomaattikastike(new Salami(new Pizzapohja())));
        
        salamipizza.describe();
        System.out.printf("hinta: %.2f € \n", salamipizza.getHinta());
        
        
    }
    
}
