/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzadecorator;

/**
 *
 * @author lauri
 */
public class Juusto extends PizzaDecorator {
    
    public Juusto(Pizza decoroitavaPizza) {
        super(decoroitavaPizza);
    }
    
    @Override
    public void decoratorDescribe(){
        System.out.println("juusto");
    }
    
    @Override
    public double getDecoratorHinta(){
        double hinta = 0;
        try {
            hinta = Double.parseDouble(properties.getProperty("juusto"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hinta;
    }
    
}
