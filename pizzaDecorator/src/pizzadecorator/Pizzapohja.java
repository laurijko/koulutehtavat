/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzadecorator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author lauri
 */
public class Pizzapohja implements Pizza {
    
    private double hinta;
    protected Properties properties = new Properties();
    
    public Pizzapohja() {
        try {
            properties.load(new FileInputStream("src/pizzadecorator/hinnasto.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        hinta = Double.parseDouble(properties.getProperty("pizzapohja"));
    }


    @Override
    public void describe() {
        System.out.println("Pizza koostuu pizzapohjasta, jossa on:");
    }

    @Override
    public double getHinta() {
        return hinta;
    }
    
}
