/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzadecorator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author lauri
 */
public abstract class PizzaDecorator implements Pizza {
    
    protected Pizza decoroitavaPizza;
    protected Properties properties = new Properties();
    
    public PizzaDecorator(Pizza decoroitavaPizza){
        this.decoroitavaPizza = decoroitavaPizza;
        try {
            properties.load(new FileInputStream("src/pizzadecorator/hinnasto.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    @Override
    public final void  describe(){
        decoroitavaPizza.describe();
        decoratorDescribe();
    }
    
    @Override
    public final double getHinta(){
        return decoroitavaPizza.getHinta() + getDecoratorHinta();
    }

    abstract void decoratorDescribe();

    abstract double getDecoratorHinta();

}
