/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzadecorator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author lauri
 */
public class Salami extends PizzaDecorator {
  
    public Salami(Pizza decoroitavaPizza) {
        super(decoroitavaPizza);
    }
    
    @Override
    public void decoratorDescribe(){
        System.out.println("salami");
    }
    
    @Override
    public double getDecoratorHinta(){
        double hinta = 0;
        try {
            hinta = Double.parseDouble(properties.getProperty("salami"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hinta;
    }
    
}
