/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzadecorator;

import java.util.Properties;

/**
 *
 * @author lauri
 */
public interface Pizza {
    
    public void describe();
    
    public double getHinta();

}
