
package observer;

import java.util.Observable;
import java.util.Observer;


public class DigitalClock implements Observer {
    ClockTimer timer;

    public DigitalClock(ClockTimer ct){
        this.timer = ct;
        timer.addObserver(this);
    }

    @Override
    public void update(Observable o, Object o1) {
        if(o == timer){
            System.out.println(o1);
        }
    }
    
    
    
    
}
