/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.Observable;

/**
 *
 * @author lauri
 */
public class ClockTimer extends Observable{
    
    private int hour = 0;
    private int minute = 0;
    private int second = 0;
    
    private void tick(){
        if(second == 59){
            second = 0;
            if(minute == 59){
                minute = 0;
                if(hour == 23){
                    hour = 0;
                } else {
                    hour++;
                }
            } else {
                minute++;
            }
        }else {
            second++;
        }
    }
    
    private String formatTime(){
        return hour+":"+minute+":"+second;
    }
    
    public void start(){
        while(true){
            tick();
            setChanged();
            notifyObservers(formatTime());
        }
    }
}
