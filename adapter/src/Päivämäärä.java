/**
 * Created by late on 25.4.2017.
 */
abstract class Päivämäärä {

    public String getPäivämäärä() {
        String päivämäärä = formatNumber(getDay())+"."+formatNumber(getMonth())+"."+formatNumber(getYear());
        return päivämäärä;
    }

    private String formatNumber(int n){
        if(n<10){
            return "0"+n;
        }
        return ""+n;
    }

    abstract int getDay();
    abstract int getMonth();
    abstract int getYear();
}
