/**
 * Created by late on 25.4.2017.
 */
public class MMDDYYAdapter extends Päivämäärä {
    private int day;
    private int month;
    private int year;

    public MMDDYYAdapter(MMDDYY mmddyy){
        String[] s = mmddyy.getPäivämäärä().split("\\.");

        day = Integer.parseInt(s[1]);
        month = Integer.parseInt(s[0]);
        year = Integer.parseInt(s[2]);


    }


    @Override
    public int getDay() {
        return day;
    }

    @Override
    public int getMonth() {
        return month;
    }

    @Override
    public int getYear() {
        return year;
    }
}
