/**
 * Created by late on 25.4.2017.
 */
public class DDMMYY extends Päivämäärä {
    private int day;
    private int month;
    private int year;

    public DDMMYY(int d, int m, int y){
        day = d;
        month = m;
        year = y;
    }

    @Override
    public int getDay() {
        return day;
    }

    @Override
    public int getMonth() {
        return month;
    }

    @Override
    public int getYear() {
        return year;
    }
}
