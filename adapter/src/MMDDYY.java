/**
 * Created by late on 25.4.2017.
 */
public class MMDDYY {
    private int day;
    private int month;
    private int year;

    public MMDDYY(int m, int d, int y){
        day = d;
        month = m;
        year = y;
    }

    public String getPäivämäärä() {
        String päivämäärä = formatNumber(month)+"."+formatNumber(day)+"."+formatNumber(year);
        return päivämäärä;
    }

    private String formatNumber(int n){
        if(n<10){
            return "0"+n;
        }
        return ""+n;
    }


}
