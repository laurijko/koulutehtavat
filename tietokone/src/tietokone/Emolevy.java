/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tietokone;

import java.util.ArrayList;

/**
 *
 * @author lauri
 */
public class Emolevy implements Koostekomponentti, Laiteosa{
    private String nimi;
    private int hinta;
    private ArrayList<Laiteosa> lista = new ArrayList();

    public Emolevy(String nimi, int hinta) {
        this.nimi = nimi;
        this.hinta = hinta;
    }

    @Override
    public void lisääLaiteosa(Laiteosa laiteosa) {
        lista.add(laiteosa);
    }

    @Override
    public int osienHinta() {
        int hinta = 0;
        for(Laiteosa laiteosa : lista){
            hinta += laiteosa.getHinta();
        }
        return hinta;
    }

    @Override
    public int getHinta() {
        return hinta+osienHinta();
    }

    @Override
    public String toString() {
        String nimet = nimi;
        for(Laiteosa osa : lista){
            nimet += ", " + osa.toString();
        }
        
        return nimet;
    }
    
    
}
