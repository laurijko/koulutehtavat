/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tietokone;

import java.util.ArrayList;

/**
 *
 * @author Laku
 */
public class Kotelo implements Koostekomponentti{
    String nimi;
    int hinta;
    ArrayList<Laiteosa> lista = new ArrayList();

    public Kotelo(String nimi, int hinta) {
        this.nimi = nimi;
        this.hinta = hinta;
    }
    
    
    @Override
    public void lisääLaiteosa(Laiteosa laiteosa) {
        lista.add(laiteosa);
    }

    @Override
    public int osienHinta() {
        int hinta = 0;
        for(Laiteosa laiteosa : lista){
            hinta += laiteosa.getHinta();
        }
        return hinta;
    }
    
    @Override
    public String toString() {
        String nimet = nimi;
        for(Laiteosa osa : lista){
            nimet += ", " + osa.toString();
        }
        
        return nimet;
    }
    
    
    
}
