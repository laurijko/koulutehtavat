/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tietokone;

/**
 *
 * @author Laku
 */
public class JimmsTehdas extends TietokoneTehdas{

    @Override
    public Kotelo rakennaTietokone() {
        Kotelo kotelo = new Kotelo("JimmsKotelo", 100);
        Emolevy emo = new Emolevy("emo", 10);
        emo.lisääLaiteosa(new CPU("i7", 200));
        emo.lisääLaiteosa(new GPU("Nvidia GTX1080ti", 300));
        emo.lisääLaiteosa(new Massamuisti("500GB SSD", 200));
        emo.lisääLaiteosa(new RAM("16GB ram", 100));
        kotelo.lisääLaiteosa(emo);
        
        return kotelo;
    }
    
}
