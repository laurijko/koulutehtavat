/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tietokone;

/**
 *
 * @author lauri
 */
public class CPU implements Laiteosa{
    String nimi;
    int hinta;

    public CPU(String nimi, int hinta) {
        this.nimi = nimi;
        this.hinta = hinta;
    }


    @Override
    public int getHinta() {
        return hinta;
    }
    
    @Override
    public String toString() {
        return nimi;
    }
}
