/**
 * Created by late on 7.5.2017.
 */
public class main {
    public static void main(String[] args) {
        Valkokangas valkokangas = new Valkokangas();
        SeinäNappi nostaNappi = new SeinäNappi(new NostaCommand(valkokangas));
        SeinäNappi laskeNappi = new SeinäNappi(new LaskeCommand(valkokangas));

        nostaNappi.push();
        laskeNappi.push();

    }
}
