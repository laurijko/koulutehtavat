/**
 * Created by late on 7.5.2017.
 */
public class LaskeCommand implements Command {
    Valkokangas valkokangas;
    public LaskeCommand(Valkokangas valkokangas) {
        this.valkokangas = valkokangas;
    }

    @Override
    public void execute() {
        valkokangas.laske();
    }
}
