/**
 * Created by late on 7.5.2017.
 */
public class NostaCommand implements Command {
    private Valkokangas valkokangas;
    public NostaCommand(Valkokangas valkokangas){
        this.valkokangas = valkokangas;
    }

    @Override
    public void execute() {
        valkokangas.nosta();
    }
}
