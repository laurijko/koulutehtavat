/**
 * Created by late on 7.5.2017.
 */
public class SeinäNappi {
    Command command;
    public SeinäNappi(Command cmd){
        command = cmd;
    }
    public void push() {
        command.execute();
    }
}
