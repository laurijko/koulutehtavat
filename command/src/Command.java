/**
 * Created by late on 7.5.2017.
 */
public interface Command {
    void execute();
}
