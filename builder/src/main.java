/**
 * Created by late on 22.4.2017.
 */
public class main {
    public static void main(String[] args) {
        BurgerBuilder hese = new HesburgerBuilder();

        hese.buildLisukkeet();
        hese.buildPihvi();
        hese.buildSämpylä();

        Burger heseBurger = hese.getBurger();
        System.out.println(heseBurger);

        BurgerBuilder mäkki = new McDonaldsBuilder();

        mäkki.buildSämpylä();
        mäkki.buildPihvi();
        mäkki.buildLisukkeet();

        Burger mäkkiBurger = mäkki.getBurger();
        System.out.println(mäkkiBurger);
    }
}
