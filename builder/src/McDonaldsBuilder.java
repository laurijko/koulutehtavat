/**
 * Created by late on 22.4.2017.
 */
public class McDonaldsBuilder extends BurgerBuilder{
    private McDonaldsBurger burger;

    public McDonaldsBuilder(){
        burger = new McDonaldsBurger();
    }

    @Override
    public Burger getBurger() {
        return burger;
    }

    @Override
    public void buildSämpylä() {
        burger.setSämpylä("McDonalds sämpylä");
    }

    @Override
    public void buildPihvi() {
        burger.setPihvi("McDonalds pihvi");

    }

    @Override
    public void buildLisukkeet() {
        burger.setLisukkeet("McDonalds juusto");
    }
}
