/**
 * Created by late on 22.4.2017.
 */
public abstract class BurgerBuilder {

    public abstract Burger getBurger();
    public abstract void buildSämpylä();
    public abstract void buildPihvi();
    public abstract void buildLisukkeet();
}
