/**
 * Created by late on 22.4.2017.
 */
public class HesburgerBuilder extends BurgerBuilder{
    private HesburgerBurger burger;

    public HesburgerBuilder(){
        burger = new HesburgerBurger();
    }

    @Override
    public Burger getBurger() {
        return burger;
    }

    @Override
    public void buildSämpylä() {
        burger.lisääOsa(new HesburgerSämpylä());
    }

    @Override
    public void buildPihvi() {
        burger.lisääOsa(new HesburgerPihvi());
    }

    @Override
    public void buildLisukkeet() {
        burger.lisääOsa(new HesburgerJuusto());
    }
}
