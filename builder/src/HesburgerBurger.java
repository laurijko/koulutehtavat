import java.util.ArrayList;

/**
 * Created by late on 22.4.2017.
 */
public class HesburgerBurger extends Burger {

    ArrayList<HesburgerOsa> osat = new ArrayList<HesburgerOsa>();

    public void lisääOsa(HesburgerOsa osa) {
        osat.add(osa);
    }

    private String osatToString(){
        String s = "";

        for(HesburgerOsa osa : osat){
            s += osa.toString() + "\n";
        }


        return s;
    }

    @Override
    public String toString() {
        return osatToString();
    }
}
