/**
 * Created by late on 22.4.2017.
 */
public class McDonaldsBurger extends Burger {
    private String sämpylä;
    private String pihvi;
    private String lisukkeet;

    public void setSämpylä(String sämpylä) {
        this.sämpylä = sämpylä;
    }

    public void setPihvi(String pihvi) {
        this.pihvi = pihvi;
    }

    public void setLisukkeet(String lisukkeet) {
        this.lisukkeet = lisukkeet;
    }

    @Override
    public String toString() {
        return sämpylä + " " + pihvi + " " + " " + lisukkeet;
    }
}
