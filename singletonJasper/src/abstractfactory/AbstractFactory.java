/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lauri
 */
public class AbstractFactory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {    
        Jasper jasper = Jasper.getInstance();
        jasper.pueVaatteet();
        jasper.päällä();
        jasper.valmistuInsinööriksi();
        jasper.pueVaatteet();
        jasper.päällä();
    }
    
}
