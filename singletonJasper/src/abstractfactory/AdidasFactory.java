/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author lauri
 */
public class AdidasFactory implements VaateFactory{

    @Override
    public Vaatekappale[] luoAsukokonaisuus() {
        Vaatekappale[] vaatekappaleet= new Vaatekappale[]{new Farmarit("Adidas"), new Lippis("Adidas"), new Kengät("Adidas"), new Teepaita("Adidas")};
        return vaatekappaleet;
    }
    
}
