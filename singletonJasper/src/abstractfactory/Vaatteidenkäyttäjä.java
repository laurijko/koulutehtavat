/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author lauri
 */
public class Vaatteidenkäyttäjä {
    
    private Lippis lippis;
    private Farmarit farmarit;
    private Kengät kengät;
    private Teepaita teepaita;
    
    public void pue(Vaatekappale vaatekappale){
        vaatekappale.pue(this);
    }

    public void setLippis(Lippis lippis) {
        this.lippis = lippis;
    }

    public void setFarmarit(Farmarit farmarit) {
        this.farmarit = farmarit;
    }

    public void setKengät(Kengät kengät) {
        this.kengät = kengät;
    }

    public void setTeepaita(Teepaita teepaita) {
        this.teepaita = teepaita;
    }
    
    public void pueVaatekokonaisuus(VaateFactory vaateFactory){
        for(Vaatekappale vaatekappale : vaateFactory.luoAsukokonaisuus()){
            pue(vaatekappale);
        }
    }
    
    public void päällä(){
        System.out.println("lippis = " + lippis.toString() + "\nt-paita = " +
                teepaita.toString() + "\nFarmarit = " + farmarit.toString() + 
                "\nkengät = " + kengät.toString());
    }
}
