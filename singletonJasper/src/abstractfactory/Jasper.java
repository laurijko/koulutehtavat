/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lauri
 */
public class Jasper extends Vaatteidenkäyttäjä implements Javakoodari {

    
            
    Class c = null;
    Class b = null;
    VaateFactory vaateFactory = null;
    VaateFactory insinööriVaateFactory = null;

    Properties properties = new Properties();
    
    private static Jasper INSTANCE = null;

    private Jasper() {
        
        try {
            properties.load(new FileInputStream("src/abstractfactory/jasper.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Class c = Class.forName(properties.getProperty("vaateFactory"));
            vaateFactory = (VaateFactory) c.newInstance();
            Class b = Class.forName(properties.getProperty("insinööriVaateFactory"));
            insinööriVaateFactory = (VaateFactory) b.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Jasper getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new Jasper();
        }
        return INSTANCE;
    }

    public void valmistuInsinööriksi() {
        System.out.println("Olen nyt insinööri!");
        vaateFactory = insinööriVaateFactory;
    }

    public void pueVaatteet() {
        this.pueVaatekokonaisuus(vaateFactory);
    }
    
    @Override
    public void koodaa(){
        System.out.println("Minä koodaan!");
    }
}
