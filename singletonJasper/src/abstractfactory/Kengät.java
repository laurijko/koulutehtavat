/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author lauri
 */
public class Kengät implements Vaatekappale {
    
    private String merkki;
    
    public Kengät(String merkki) {
        this.merkki = merkki;
    }
    
    public void pue(Vaatteidenkäyttäjä vaatteikenkäyttäjä){
        vaatteikenkäyttäjä.setKengät(this);
    }
    
    public String toString(){
        return merkki + " kengät";
    }
}
