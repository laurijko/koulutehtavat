/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author lauri
 */
public class Lippis implements Vaatekappale {
    
    private String merkki;
    
    public Lippis(String merkki){
        this.merkki = merkki;
    }
    
    public String toString(){
        return merkki + " lippis";
    }

    @Override
    public void pue(Vaatteidenkäyttäjä vaatteidenkäyttäjä) {
        vaatteidenkäyttäjä.setLippis(this);
    }
}
