/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author lauri
 */
public class Teepaita implements Vaatekappale{
    private String merkki;
    
    public Teepaita(String merkki){
        this.merkki = merkki;
    }
    
    public String toString(){
        return merkki + " t-paita";
    }

    @Override
    public void pue(Vaatteidenkäyttäjä vaatteidenkäyttäjä) {
        vaatteidenkäyttäjä.setTeepaita(this);
    }
}
