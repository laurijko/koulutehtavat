/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author lauri
 */
public class BossFactory implements VaateFactory {

    @Override
    public Vaatekappale[] luoAsukokonaisuus() {
        Vaatekappale[] vaatekappaleet= new Vaatekappale[]{new Farmarit("Boss"), new Lippis("Boss"), new Kengät("Boss"), new Teepaita("Boss")};
        return vaatekappaleet;
    }
    
}
