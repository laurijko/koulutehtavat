/**
 * Created by late on 16.4.2017.
 */
public class Päällikkö extends Palkankorotus {
    @Override
    public void käsittelePyyntö(int korotusProsentti) {
        if(korotusProsentti<=5){
            System.out.println("Päällikkö hyväksyy palkankorotuksen.");
        } else {
            successor.käsittelePyyntö(korotusProsentti);
        }
    }
}
