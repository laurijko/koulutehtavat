/**
 * Created by late on 16.4.2017.
 */
public class Esimies extends Palkankorotus {
    @Override
    public void käsittelePyyntö(int korotusProsentti) {
        if(korotusProsentti<=2){
            System.out.println("Lähiesimies hyväksyy palkankorotuksen.");
        } else {
            successor.käsittelePyyntö(korotusProsentti);
        }
    }
}
