import java.util.Scanner;

/**
 * Created by late on 11.4.2017.
 */
public class KPS extends Game {
    private Scanner s = new Scanner(System.in);

    private boolean end;

    private int winner;

    private enum Move {
        KIVI, PAPERI, SAKSET;

        public int compareMoves(Move otherMove) {
            // tasapeli
            if (this == otherMove)
                return 0;

            switch (this) {
                case KIVI:
                    return (otherMove == SAKSET ? 1 : -1);
                case PAPERI:
                    return (otherMove == KIVI ? 1 : -1);
                case SAKSET:
                    return (otherMove == PAPERI ? 1 : -1);
            }

            // Should never reach here
            return 0;
        }
    }

    private Move move;

    @Override
    void initializeGame() {
        end = false;
        move = null;
        winner = 0;
    }

    @Override
    void makePlay(int player) {
        boolean ok;
        do {
            System.out.println("Pelaaja " + (player+1) + ":");
            System.out.println("Anna kivi, paperi tai sakset.");
            String input = s.nextLine();
            System.out.println("");

            ok = true;
            switch (input.toUpperCase()) {
                case "KIVI":
                    setMove(Move.KIVI);
                    break;
                case "PAPERI":
                    setMove(Move.PAPERI);
                    break;
                case "SAKSET":
                    setMove(Move.SAKSET);
                    break;
                case "SHOTGUN":
                    System.out.println("Ei saa huijjata!");
                    ok = false;
                    break;
                default:
                    System.out.print("Väärä syöte!");
                    ok = false;
                    break;
            }
        }while(!ok);
    }

    private void setMove(Move m){
        if(move == null){
            move = m;
        } else {
            switch (m.compareMoves(move)) {
                case 0: // Tasapeli
                    System.out.println("Tasapeli!");
                    break;
                case 1: // 2 pelaaja voitti
                    System.out.println(m + " voittaa " + move + "!");
                    winner = 2;
                    break;
                case -1: // 1 pelaaja voitti
                    System.out.println(move + " voittaa " + m + "!");
                    winner = 1;
                    break;
            }
            end = true;
        }
    }

    @Override
    boolean endOfGame() {
        return end;
    }

    @Override
    void printWinner() {
        if(winner!=0){
            System.out.println("Pelaaja " + winner + " voitti!");
        }
    }
}
