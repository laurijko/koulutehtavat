import java.util.Scanner;

/**
 * Created by late on 17.4.2017.
 */
public class Arvaaja {
    private Arvuuttaja arvuuttaja;
    private Arvauslappu arvauslappu;
    private Scanner s = new Scanner(System.in);
    public void arvaa(){
        while(arvauslappu!=null) {
            System.out.println("Arvaa numero 1-10");
            int numero = s.nextInt();
            arvauslappu.arvaa(numero);
            arvauslappu = arvuuttaja.aarvaa(arvauslappu);
        }
    }

    public Arvaaja(Arvuuttaja arvuuttaja){
        this.arvuuttaja = arvuuttaja;
        this.arvauslappu = arvuuttaja.liityPeliin();
    }

}
