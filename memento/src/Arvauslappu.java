/**
 * Created by late on 17.4.2017.
 */
public class Arvauslappu {
    private Arvuuttaja arvuuttaja;
    private int numero;
    int arvaus;

    public Arvauslappu(int numero, Arvuuttaja arvuuttaja){
        this.numero = numero;
        this.arvuuttaja = arvuuttaja;
    }

    public void arvaa(int numero){
        this.arvaus=numero;
    }

    public void getNumero(){
        arvuuttaja.setNumero(numero);
    }
}
