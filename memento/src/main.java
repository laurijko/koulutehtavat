import java.util.Scanner;


public class main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Arvuuttaja arvuuttaja = new Arvuuttaja();
        Arvaaja arvaaja = new Arvaaja(arvuuttaja);

        arvaaja.arvaa();
    }
}
