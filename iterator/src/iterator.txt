Iteraattori:
Kun säikeille antaa omat iteraattorit, ne molemmat käyvät listan kaikki alkiot läpi.
Kun säikeille antaa samat iteraattorit, niin kumpikin käy vain osan listan alkioista läpi.
Kun listaan tehdään läpikäynnin aikana muutoksia niin tulee java.util.ConcurrentModificationException.