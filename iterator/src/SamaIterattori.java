import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Laku on 6.5.2017.
 */
public class SamaIterattori {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList();
        int listanKoko = 50;
        for(int i=0; i<listanKoko; i++){
            lista.add(i);
        }
        Iterator iterator = lista.iterator();
        Säie eka = new Säie(iterator, "eka");
        Säie toka = new Säie(iterator,"toka");

        eka.start();
        toka.start();

    }
}

