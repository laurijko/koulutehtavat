import java.util.Iterator;

/**
 * Created by Laku on 6.5.2017.
 */
public class Säie extends Thread {
    private Iterator iterator;
    private String nimi;
    public Säie(Iterator i, String nimi){
        iterator = i;
        this.nimi = nimi;
    }

    public void run(){
        synchronized (this) {
            while(iterator.hasNext()){
                System.out.println(iterator.next().toString() + " " + nimi);

                try {
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
