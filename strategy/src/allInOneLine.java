import java.util.Iterator;
import java.util.List;

/**
 * Created by late on 14.4.2017.
 */
public class allInOneLine implements ListConverterStrategy {

    @Override
    public String listToString(List l) {
        String s = "";
        Iterator i = l.iterator();
        while(i.hasNext()) {
            s += i.next().toString();
            s += "\\n";
        }
        return s;
    }
}
