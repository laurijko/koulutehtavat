import java.util.List;

/**
 * Created by late on 14.4.2017.
 */
public class everyThirdNewLine implements ListConverterStrategy {
    @Override
    public String listToString(List l) {
        String s = "";
        for(int i = 0; i < l.size(); i++){
            if(i%3==0)
                s+= "\n";
            s += l.get(i).toString();
        }
        return s;
    }
}
