import java.util.List;

/**
 * Created by late on 14.4.2017.
 */
public class ListConverter {
    private ListConverterStrategy strategy;

    public ListConverter(ListConverterStrategy s){
        strategy = s;
    }

    public String listToString(List l){
        return strategy.listToString(l);
    }
}
