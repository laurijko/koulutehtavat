
import java.lang.reflect.Array;
import java.util.List;

/**
 * Created by late on 14.4.2017.
 */
public class everyOtherNewLine implements ListConverterStrategy {
    @Override
    public String listToString(List l) {
        Object[] taulukko;
        taulukko = l.toArray();
        String s = "";
        for(int i = 0; i<taulukko.length; i++) {
            if(i%2 == 0)
                s += "\n";
            s += taulukko[i].toString();
        }

        return s;
    }
}
