import java.util.List;

/**
 * Created by late on 14.4.2017.
 */
public interface ListConverterStrategy {
    public String listToString(List l);
}
