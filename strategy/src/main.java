import java.util.ArrayList;
import java.util.List;

/**
 * Created by late on 14.4.2017.
 */
public class main {
    public static void main(String[] args) {

        List l = new ArrayList();
        l.add("eka");
        l.add("toka");
        l.add("kolmas");
        l.add("neljäs");
        l.add("viides");
        l.add("kuudes");
        l.add("seittemäs");
        l.add("kahdeksas");
        l.add("yhdeksäs");
        l.add("kymmenes");
        l.add("yhdestoista");
        l.add("kahdestoista");
        l.add("kolmastoista");
        l.add("neljästoista");
        l.add("viidestoista");
        l.add("kuudestoista");
        l.add("viidestoiosta");
        l.add("kuudestoista");
        l.add("seittemästoista");
        l.add("kahdeksastoista");
        l.add("yhdeksästoista");
        l.add("kahdeskymmenes");

        ListConverter ls;
        ls = new ListConverter(new allInOneLine());
        System.out.println(ls.listToString(l));

        ls = new ListConverter(new everyOtherNewLine());
        System.out.println(ls.listToString(l));

        ls = new ListConverter(new everyThirdNewLine());
        System.out.println(ls.listToString(l));
    }
}
